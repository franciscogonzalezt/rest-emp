# Rest Empleados

## Descripcion

API REST con integración de métodos GET, POST, PUT, DELETE y PATCH
y control de respuestas HTTP.

## Ejecucion

### Compile and Start

```
mvn clean package spring-boot:start
```

### Con ayuda de IntelliJ

```
En el menu de Maven:
    1. Lifecycle -> Package
    2. spring-boot -> spring-boot:stop
    2. spring-boot -> spring-boot:start
```

## Ejemplo de Uso
Probar con Postman, para mas ejemplos acceder a documentacion de Swagger en el link indicado mas adelante.

### GET

Ejemplo de metodo GET

URL:
```
http://localhost:8080/api/empleados/
```

Ejemplo de Respuesta:

```json
{
  "listaEmpleados": [
    {
      "id": 1,
      "nombre": "Antonio",
      "apellido": "Lopez",
      "email": "antonio@lopez.com",
      "capacitaciones": [
        {
          "fecha": "2019/01/01",
          "titulo": "DBA"
        }
      ]
    }
  ]
}
```

## Documentacion con Swagger
Se hace uso de la version 3.0.0 de la dependencia io.springfox
Entrar a la siguiente URL para ver documentacion:
```
http://localhost:8080/api/swagger-ui/
http://localhost:8080/api/v2/api-docs
```