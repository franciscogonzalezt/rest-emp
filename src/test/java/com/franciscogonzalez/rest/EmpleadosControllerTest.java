package com.franciscogonzalez.rest;

import com.franciscogonzalez.rest.utils.BadSeparator;
import com.franciscogonzalez.rest.utils.EstadosPedido;
import com.franciscogonzalez.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testGetCadena() {
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "Luz del Sol";
        assertEquals(correcto, empleadosController.getCadena(origen, "."));
    }

    @Test
    public void testGetAutor() {
        assertEquals("Francisco Gonzalez", empleadosController.getAppAutor());
    }

    @Test
    public void testSeparadorGetCadena() {
        try {
            Utilidades.getCadena("Francisco Gonzalez", "..");
            fail("Se esperaba BadSeparator");
        } catch (BadSeparator e) {
            assertTrue(true);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, 15})
    public void testEsImpar(int numero) {
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void testEstaBlando(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void testEstaBlancoCompleto(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep) {
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }
}
