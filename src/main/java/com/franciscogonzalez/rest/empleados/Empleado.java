package com.franciscogonzalez.rest.empleados;

import java.util.ArrayList;

public class Empleado {
    private int id;
    private String nombre;
    private String apellido;
    private String email;
    private ArrayList<Capacitacion> capacitaciones;

    public Empleado() {
    }

    public Empleado(int id, String nombre, String apellido, String email, ArrayList<Capacitacion> capacitaciones) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.capacitaciones = capacitaciones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Capacitacion> getCapacitaciones() {
        return capacitaciones;
    }

    public void setCapacitaciones(ArrayList<Capacitacion> capacitaciones) {
        this.capacitaciones = capacitaciones;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", email='" + email + '\'' +
                ", capacitaciones=" + capacitaciones +
                '}';
    }
}
