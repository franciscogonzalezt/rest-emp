package com.franciscogonzalez.rest;

import com.franciscogonzalez.rest.empleados.Capacitacion;
import com.franciscogonzalez.rest.empleados.Empleado;
import com.franciscogonzalez.rest.empleados.Empleados;
import com.franciscogonzalez.rest.repositorios.EmpleadoDAO;
import com.franciscogonzalez.rest.utils.BadSeparator;
import com.franciscogonzalez.rest.utils.Configuracion;
import com.franciscogonzalez.rest.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {
    @Autowired
    private EmpleadoDAO empDao;
    @Autowired
    Configuracion configuracion;

    @GetMapping(path = "/")
    public Empleados getEmpleados() {
        return empDao.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id) {
        Empleado emp = empDao.getEmpleado(id);
        if (emp == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(emp);
        }
    }

    @PostMapping(path = "/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        int id = empDao.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setId(id);
        empDao.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(emp.getId())
                        .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateEmpleado(@RequestBody Empleado emp) {
        empDao.updateEmpleado(emp);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateEmpleado(@PathVariable int id, @RequestBody Empleado emp) {
        empDao.updateEmpleado(id, emp);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteEmpleado(@PathVariable int id) {
        String respuesta = empDao.deleteEmpleado(id);
        if (respuesta.equals("OK"))
            return ResponseEntity.ok().build();
        else
            return ResponseEntity.notFound().build();
    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object>  softUpdateEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates) {
        empDao.softUpdateEmpleado(id, updates);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int id) {
        return ResponseEntity.ok().body(empDao.getCapacitacionesEmpleado(id));
    }

    @PostMapping(path = "/{id}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addCapacitacionEmpleado(@PathVariable int id, @RequestBody Capacitacion cap) {
        return empDao.addCapacitacion(id, cap) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }

    @GetMapping(path = "/titulo")
    public String getAppTitulo() {
        String modo = configuracion.getModo();
        String titulo = configuracion.getTitulo();
        return String.format("%s (%s)", titulo, modo);
    }

    @GetMapping(path = "/autor")
    public String getAppAutor() {
        return configuracion.getAutor();
    }

    @GetMapping(path = "/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador) {
        try {
            return Utilidades.getCadena(texto, separador);
        } catch (BadSeparator e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    /* Para acceder a las variables en el archivo de propiedades tambien se puede hacer de la siguiente forma:
    @Autowired
    private Environment env;
    env.getProperty("app.autor");

    @Value("${app.titulo}")
    String titulo;
     */
}
