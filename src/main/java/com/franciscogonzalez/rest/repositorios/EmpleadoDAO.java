package com.franciscogonzalez.rest.repositorios;

import com.franciscogonzalez.rest.empleados.Capacitacion;
import com.franciscogonzalez.rest.empleados.Empleado;
import com.franciscogonzalez.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadoDAO {
    Logger LOGGER = LoggerFactory.getLogger(EmpleadoDAO.class);
    private static Empleados list = new Empleados();
    static{
        Capacitacion cap1 = new Capacitacion("2019/01/01", "DBA");
        Capacitacion cap2 = new Capacitacion("2020/01/01", "Backend");
        Capacitacion cap3 = new Capacitacion("2021/01/01", "Frontend");
        ArrayList<Capacitacion> una = new ArrayList<>();
        una.add(cap1);
        ArrayList<Capacitacion> dos = new ArrayList<>();
        dos.add(cap1);
        dos.add(cap2);
        ArrayList<Capacitacion> todas = new ArrayList<>(dos);
        todas.add(cap3);
        list.getListaEmpleados().add(new Empleado(1, "Antonio", "Lopez", "antonio@lopez.com", una));
        list.getListaEmpleados().add(new Empleado(2, "Pedro", "Perez", "pedro@perez.com", dos));
        list.getListaEmpleados().add(new Empleado(3, "Luis", "Marquez", "luis@marquez.com", todas));
    }

    public Empleados getAllEmpleados() {
        LOGGER.debug("Ejecucion de metodo 'getAllEmpleados'");
        return list;
    }

    public Empleado getEmpleado(int id) {
        for(Empleado emp : list.getListaEmpleados()) {
            if(emp.getId() == id) {
                return emp;
            }
        }

        return null;
    }

    public void addEmpleado(Empleado emp) {
        list.getListaEmpleados().add(emp);
    }

    public void updateEmpleado(Empleado emp) {
        Empleado current = getEmpleado(emp.getId());
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }

    public void updateEmpleado(int id, Empleado emp) {
        Empleado current = getEmpleado(id);
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }

    public String deleteEmpleado(int id) {
        Empleado current = getEmpleado(id);
        if(current == null) return "NO";

        Iterator it = list.getListaEmpleados().listIterator();
        while(it.hasNext()) {
            Empleado emp = (Empleado) it.next();
            if(emp.getId() == id) {
                it.remove();
                break;
            }
        }
        return "OK";
    }

    public void softUpdateEmpleado(int id, Map<String, Object> updates) {
        Empleado current = getEmpleado(id);
        for (Map.Entry<String, Object> update : updates.entrySet()) {
            switch (update.getKey()) {
                case "nombre":
                    current.setNombre(update.getValue().toString());
                    break;
                case "apellido":
                    current.setApellido(update.getValue().toString());
                    break;
                case "email":
                    current.setEmail(update.getValue().toString());
                    break;
            }
        }
    }

    public List<Capacitacion> getCapacitacionesEmpleado(int id) {
        Empleado current = getEmpleado(id);
        ArrayList<Capacitacion> caps = new ArrayList<>();
        if (current != null)
            caps = current.getCapacitaciones();
        return caps;
    }

    public boolean addCapacitacion (int id, Capacitacion cap) {
        Empleado current = getEmpleado(id);
        if (current == null) return false;

        current.getCapacitaciones().add(cap);
        return true;
    }
}
